export function renderTime(date) {
  const dateee = new Date(date).toJSON()
  return new Date(+new Date(dateee) + 16 * 3600 * 1000).toISOString().replace(/T/g, ' ').replace(/\.[\d]{3}Z/, '')
}

export function betweenDate(date, rangeDate) {
  date = new Date(date)
  const date1 = new Date(rangeDate[0])
  const date2 = new Date(rangeDate[1])
  return (
    date.getTime() - date1.getTime() > 0 && date.getTime() - date2.getTime() < 0
  )
}

export function hour(date) {
  const dateee = new Date(date).toJSON()
  return new Date(+new Date(dateee) + 8 * 3600 * 1000).toISOString().replace(/T/g, ' ').replace(/\.[\d]{3}Z/, '').substr(10, 6)
}

export function changeTime(time) {
  var dateTimeStamp = new Date(time).getTime()
  var minute = 1000 * 60
  var hour = minute * 60
  var day = hour * 24

  var month = day * 30
  var year = month * 12
  var now = new Date().getTime()
  var diffValue = now - dateTimeStamp

  var result = ''
  if (diffValue < 0) {
    return
  }

  var monthC = diffValue / month
  var weekC = diffValue / (7 * day)
  var dayC = diffValue / day
  var hourC = diffValue / hour
  var minC = diffValue / minute
  var yearC = diffValue / year
  if (yearC >= 1) {
    return '' + parseInt(yearC) + '年前'
  }
  if (monthC >= 1) {
    result = '' + parseInt(monthC) + '月前'
  } else if (weekC >= 1) {
    result = '' + parseInt(weekC) + '周前'
  } else if (dayC >= 1) {
    result = '' + parseInt(dayC) + '天前'
  } else if (hourC >= 1) {
    result = '' + parseInt(hourC) + '小时前'
  } else if (minC >= 1) {
    result = '' + parseInt(minC) + '分钟前'
  } else {
    result = '刚刚'
  }

  return result
}
export function timeFormat(secondes) {
  // 初始判断
  if (secondes < 0) {
    return '不详'
  }
  var minute = 60
  var hour = minute * 60
  var day = hour * 24
  var month = day * 30
  var year = month * 12
  var result = ''
  var monthC = secondes / month
  var weekC = secondes / (7 * day)
  var dayC = secondes / day
  var hourC = secondes / hour
  var minC = secondes / minute
  var yearC = secondes / year
  // 转换判断
  if (yearC >= 1) {
    return '' + parseInt(yearC) + '年前'
  }
  if (monthC >= 1) {
    result = '' + parseInt(monthC) + '月前'
  } else if (weekC >= 1) {
    result = '' + parseInt(weekC) + '周前'
  } else if (dayC >= 1) {
    result = '' + parseInt(dayC) + '天前'
  } else if (hourC >= 1) {
    result = '' + parseInt(hourC) + '小时前'
  } else if (minC >= 1) {
    result = '' + parseInt(minC) + '分钟前'
  } else {
    result = '刚刚'
  }
  return result
}
// 将当前时间的时间戳转化为 M-D格式的时间
// 参数为毫秒数
export function secondsToDate(seconds) {
	
  if(seconds == null || seconds == '') return ''
  var dateee = new Date(seconds).toJSON()
  return new Date(+new Date(dateee) + 8 * 3600 * 1000).toJSON().replace(/T/g, ' ').replace(/\.[\d]{3}Z/, '').substr(5,6)
}

// 将当前时间的时间戳转化为 y-M-D格式的时间
// 参数为毫秒数
export function secondsToYMD(seconds) {
  if(seconds == null || seconds == '') return ''
  var dateee = new Date(seconds).toJSON()
  return new Date(+new Date(dateee) + 8 * 3600 * 1000).toJSON().replace(/T/g, ' ').replace(/\.[\d]{3}Z/, '').substr(0, 10)
}

// 将当前时间的时间戳转化为 y-M-D格式的时间
// 参数为毫秒数
export function secondsToAllDate(seconds) {
  if(seconds == null || seconds == '') return ''
  var dateee = new Date(seconds).toJSON()
  return new Date(+new Date(dateee) + 8 * 3600 * 1000).toJSON().replace(/T/g, ' ').replace(/\.[\d]{3}Z/, '')
}



// 文件大小换算,字节转换GB
export function bytesToSize(bytes) {
  if (bytes == null) return ''
  if (bytes === 0) return '0 B'

  const k = 1024

  const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

  const i = Math.floor(Math.log(bytes) / Math.log(k))

  const num = bytes / Math.pow(k, i)
  return num.toFixed(2) + ' ' + sizes[i]

  // return (bytes / Math.pow(k, i)) + ' ' + sizes[i];
  // toPrecision(3) 后面保留一位小数，如1.0GB //return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
}



export default {
  changeTime,
  secondsToDate,
  bytesToSize,
  renderTime,
  timeFormat,
  betweenDate,
  secondsToYMD,
  secondsToAllDate
 }
