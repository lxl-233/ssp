import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);//在创建Vue实例之前

export default new Vuex.Store({
    state: {
        //存放组件之间共享的数据
        userInfo: uni.getStorageSync('userInfo'),
        hasLogin: uni.getStorageSync('userInfo')  ? true : false,
    },
	actions: {
	    // 退出
	    logout({commit, state}) {
			state.userInfo = null
			state.hasLogin = false
	    },
	
		login({state}, data) {
			state.userInfo = data
			state.hasLogin = true
		}
	}
})