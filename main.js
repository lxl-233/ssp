import Vue from 'vue'
import App from './App'
import store from './store'
import utils from './utils/utils'
Vue.config.productionTip = false
App.mpType = 'app'
Vue.prototype.$utils = utils
const app = new Vue({
	store,
    ...App
})
app.$mount()